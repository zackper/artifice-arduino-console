#include <Arduino.h>
#include <Wire.h>
#include <Button.h>
#include <SlideSwitch.h>
#include <Potentiometer.h>
#include <LiquidCrystal_I2C.h>
#include <InputManager.h>
#include <ArtificeMenu.h>
#include <ArtificeMenu_Home.h>
#include <ArtificeMenu_IntroScreen.h>
#include <ArtificeLcdCharacters.h>
#include <Character.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C *lcd;

// Menus
ArtificeMenu *menu;

// Update times
unsigned long lastInputUpdate = 0;
unsigned long lastDisplayUpdate = 0;
unsigned long lastSoundUpdate = 0;
byte inputUpdateInterval = 65;
byte displayUpdateInterval = 65;
byte soundUpdateInterval = 50;

void InitializeCharacter()
{
    auto luther = new Character();
    strcpy(luther->characterName, "Luther Smith");
    strcpy(luther->className, "Artificer");

    // Set character ID
    luther->characterId = LUTHER_ID;

    // Set HP
    luther->hp->current = 50;
    luther->hp->max = 56;

    // // Set up spell slots
    luther->spellSlots.PushBack(new SpellSlot(1, 5, 8)); // First level spell slots
    luther->spellSlots.PushBack(new SpellSlot(2, 3, 5)); // Second level spell slots
    luther->spellSlots.PushBack(new SpellSlot(3, 1, 4)); // Third level spell slots
    luther->spellSlots.PushBack(new SpellSlot(4, 2, 2)); // Forth level spell slots
    luther->spellSlots.PushBack(new SpellSlot(5, 0, 0)); // Fifth level spell slots
    luther->spellSlots.PushBack(new SpellSlot(6, 0, 0)); // Sixith level spell slots
    luther->spellSlots.PushBack(new SpellSlot(7, 0, 0)); // Seventh level spell slots
    luther->spellSlots.PushBack(new SpellSlot(8, 0, 0)); // Eigth level spell slots
    luther->spellSlots.PushBack(new SpellSlot(9, 0, 0)); // Nineth level spell slots

    Serial.println("Lol");

    // // Set bardic points
    luther->bardicPoints = 0;

    luther->Load();

    // // luther->Save();

    MainCharacter::GetInstance()->SetCharacter(luther);
}
void InitializeLcdMenus()
{
    menu = new ArtificeMenu_IntroScreen();
}
void InitializeLcdI2CCharacters()
{
    uint8_t fullBlock[8] = {
        0x1F,
        0x1F,
        0x1F,
        0x1F,
        0x1F,
        0x1F,
        0x1F,
        0x1F};
    uint8_t indicator[8] = {
        0x03,
        0x05,
        0x09,
        0x11,
        0x11,
        0x09,
        0x05,
        0x03};
    uint8_t hpIcon[8] = {
        0x00,
        0x0A,
        0x15,
        0x11,
        0x0A,
        0x04,
        0x00,
        0x00};
    uint8_t spellsIcon[8] = {
        0x04,
        0x02,
        0x08,
        0x04,
        0x0A,
        0x1E,
        0x1F,
        0x0C};

    lcd->createChar(FULL_BLOCK_INDEX, fullBlock);
    lcd->createChar(INDICATOR_INDEX, indicator);
    lcd->createChar(HP_ICON_INDEX, hpIcon);
    lcd->createChar(SPELLS_ICON_INDEX, spellsIcon);
}
void InitializeLcdI2C()
{
    // Initialize LCD i2C screen
    lcd = new LiquidCrystal_I2C(0x27, 16, 2);
    lcd->begin();
    lcd->backlight();
    InitializeLcdI2CCharacters();
}
void InitializeInputManager()
{
    Button *upButton = new Button(2);
    InputManager::getInstance().AddInput(InputType::Up, upButton);

    Button *downButton = new Button(3);
    InputManager::getInstance().AddInput(InputType::Down, downButton);

    Button *bButton = new Button(4);
    InputManager::getInstance().AddInput(InputType::B, bButton);

    Button *aButton = new Button(5);
    InputManager::getInstance().AddInput(InputType::A, aButton);

    SlideSwitch *soundSwitch = new SlideSwitch(6);
    InputManager::getInstance().AddInput(InputType::SoundSwitch, soundSwitch);
}
void InitializeSoundManager()
{
    // Quick and dirty to make sure buzzer works
    // pinMode(6, INPUT);
    pinMode(7, OUTPUT);
}

void InputLoop()
{
    InputManager::getInstance().UpdateInput();
}
void DisplayLoop()
{
    menu->Update(lcd);
}

void setup()
{
    InitializeCharacter();
    InitializeLcdI2C();
    InitializeLcdMenus();
    InitializeInputManager();

    // Start serial connection
    Serial.begin(9600);

    // Set initial display
    menu->Display(lcd);
}
void loop()
{
    auto currentTime = millis();
    if (currentTime - lastInputUpdate > inputUpdateInterval)
    {
        InputLoop();
        lastInputUpdate = currentTime;
    }
    if (currentTime - lastDisplayUpdate > displayUpdateInterval)
    {
        DisplayLoop();
        lastDisplayUpdate = currentTime;
    }
    if (currentTime - lastSoundUpdate > soundUpdateInterval)
    {
        SoundManager::getInstance().Update(currentTime);
        lastSoundUpdate = currentTime;
    }
}