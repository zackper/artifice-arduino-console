#ifndef ARTIFICE_MENU_LCD_MENU_H
#define ARTIFICE_MENU_LCD_MENU_H

#include <Arduino.h>
#include <ArtificeMenu.h>
#include <ArtificeMenu_HpCounter.h>
#include <ArtificeMenu_SpellSlots.h>
#include <ArtificeMenu_Settings.h>
#include <LiquidCrystal_I2C.h>

class ArtificeMenu_Home : public ArtificeMenu
{
public:
    ArtificeMenu_Home() : ArtificeMenu()
    {
        SetTitle("Home Screen");
        InitializeSubmenus();
    }

private:
    void InitializeSubmenus()
    {
        AddSubmenu(new ArtificeMenu_HpCounter());
        AddSubmenu(new ArtificeMenu_SpellSlots());
        AddSubmenu(new ArtificeMenu_Settings());
    }
};

#endif