#ifndef HP_COUNTER_LCD_MENU_H
#define HP_COUNTER_LCD_MENU_H

#include <Arduino.h>
#include <ArtificeMenu.h>
#include <LiquidCrystal_I2C.h>
#include <ArtificeLcdCharacters.h>
#include <Character.h>

class ArtificeMenu_HpCounter : public ArtificeMenu
{
public:
    HpCounter *hp;

    ArtificeMenu_HpCounter() : ArtificeMenu()
    {
        SetTitle("HP Counter");
    }

    void Display(LiquidCrystal_I2C *lcd) override
    {
        hp = MainCharacter::GetInstance()->GetCharacter()->hp;

        lcd->clear();
        if (!isEditing) // Restore blink, if I was previously editting
            lcd->blink_off();

        DisplayFirstLine(lcd);
        if (!isEditing)
            DisplayHpBar(lcd);
        else
            DisplayHpChange(lcd);
    }
    void IndicatePrevious() override
    {
        if (isEditing)
            changeAmount += 1;
        else
            ArtificeMenu::IndicatePrevious();
    }
    void IndicateNext() override
    {
        if (isEditing)
            changeAmount -= 1;
        else
            ArtificeMenu::IndicateNext();
    }
    void Select() override
    {
        if (!isEditing)
            isEditing = true;
        else
        {
            ApplyChange();
            MainCharacter::GetInstance()->GetCharacter()->Save();
            isEditing = false;
        }
    }
    void Back() override
    {
        if (isEditing)
        {
            isEditing = false;
            changeAmount = 0;
        }
        else
            ArtificeMenu::Back();
    }

private:
    bool isEditing = false;
    int8_t changeAmount = 0;

    void DisplayFirstLine(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(0, 0);
        lcd->print("HP: ");
        lcd->print(hp->current);
        lcd->print("/");
        lcd->print(hp->max);
    }
    void DisplayHpBar(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(0, 1);
        lcd->print("[");

        auto percent = hp->GetPercent();
        auto barsCount = map(percent, 0, 100, 0, 14);
        for (byte i = 0; i < barsCount; i++)
        {
            lcd->write(FULL_BLOCK_INDEX);
        }

        lcd->setCursor(15, 1);
        lcd->print("]");

        // If editting, set blinking to last bar
        if (isEditing)
        {
            // Dont allow it to be further than screen. Either left or right
            byte index = min(barsCount + 1, 15);
            index = max(index, 1);
            lcd->setCursor(index, 1);
            lcd->blink();
        }
    }
    void DisplayHpChange(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(0, 1);
        lcd->print("Change: ");
        lcd->print(changeAmount);
        lcd->blink_on();
    }

    void ApplyChange()
    {
        if(changeAmount > 0){
            SoundManager::getInstance().RestoreHP();
        }
        else{
            SoundManager::getInstance().LoseHP();
        }

        hp->current += changeAmount;
        changeAmount = 0;
    }
};

#endif