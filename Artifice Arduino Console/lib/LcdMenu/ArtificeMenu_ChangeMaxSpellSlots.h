#ifndef ARTIFICE_MENU_CHANGE_MAX_SPELL_SLOTS_H
#define ARTIFICE_MENU_CHANGE_MAX_SPELL_SLOTS_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <ArtificeMenu.h>
#include <Character.h>

class ArtificeMenu_ChangeMaxSpellSlots : public ArtificeMenu
{
public:
    ArtificeMenu_ChangeMaxSpellSlots() : ArtificeMenu()
    {
        SetTitle("Spell Slots");

        auto character = MainCharacter::GetInstance()->GetCharacter();
        auto spellSlots = &character->spellSlots;

        Serial.println("Spell Slot");
        for (size_t i = 0; i < spellSlots->GetSize(); i++)
        {
            Serial.print("Level ");
            Serial.println(i);
            char name[16] = "Level: \0";
            name[7] = ((byte)i + 1) + '0';
            AddSubmenu(new ArtificeMenu_ChangeByteValue(name, &((*spellSlots)[i]->maxSlots)));
        }
    }

};

#endif