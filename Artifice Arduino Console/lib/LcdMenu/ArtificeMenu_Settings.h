#ifndef ARTIFICE_MENU_SETTINGS_H
#define ARTIFICE_MENU_SETTINGS_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <ArtificeMenu.h>
#include <Character.h>
#include <ArtificeMenu_ChangeByteValue.h>
#include <ArtificeMenu_ChangeInt16Value.h>
#include <ArtificeMenu_ChangeMaxSpellSlots.h>


class ArtificeMenu_Settings : public ArtificeMenu
{
public:
    ArtificeMenu_Settings() : ArtificeMenu()
    {
        auto character = MainCharacter::GetInstance()->GetCharacter();

        SetTitle("Settings");
        AddSubmenu(new ArtificeMenu_ChangeByteValue("Level", &character->level));
        AddSubmenu(new ArtificeMenu_ChangeInt16Value("Max Hp", &character->hp->max));
        // AddSubmenu(new ArtificeMenu_ChangeMaxSpellSlots());
        AddSubmenu(new ArtificeMenu_ChangeByteValue("Character ID", &character->characterId));
    }

    void Back() override{
        if(isSubmenuOpen)
        {
            MainCharacter::GetInstance()->GetCharacter()->Save();
            MainCharacter::GetInstance()->GetCharacter()->Load();
            ArtificeMenu::Back();
            return;
        }

        ArtificeMenu::Back();
    }

private:
};

#endif