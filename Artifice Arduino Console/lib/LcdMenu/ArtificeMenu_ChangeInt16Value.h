#ifndef ARTIFICE_MENU_CHANGE_INT16_VALUE_H
#define ARTIFICE_MENU_CHANGE_INT16_VALUE_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <ArtificeMenu.h>
#include <Character.h>

class ArtificeMenu_ChangeInt16Value : public ArtificeMenu
{
public:
    ArtificeMenu_ChangeInt16Value(const char *name, int16_t *value) : ArtificeMenu()
    {
        SetTitle(name);
        this->name = name;
        this->value = value;
    }

    void Display(LiquidCrystal_I2C *lcd) override
    {
        ClearLcd(lcd);
        lcd->blink_on();
        DisplayFirstLine(lcd);
        DisplaySecondLine(lcd);
    }

    void Back() override
    {
        ArtificeMenu::Back();
        MainCharacter::GetInstance()->GetCharacter()->Save();
    }
    void IndicateNext() override
    {
        *value -= 1;
    }
    void IndicatePrevious() override
    {
        *value += 1;
    }

private:
    const char *name;
    int16_t *value;

    void DisplayFirstLine(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(0, 0);
        lcd->print(name);
    }
    void DisplaySecondLine(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(0, 1);
        lcd->print("Value: ");
        lcd->print(*value);
    }
};

#endif