#ifndef SPELL_SLOTS_LCD_MENU_H
#define SPELL_SLOTS_LCD_MENU_H

#include <Arduino.h>
#include <SimpleVector.h>
#include <ArtificeMenu.h>
#include <LiquidCrystal_I2C.h>
#include <Character.h>

class ArtificeMenu_SpellSlots : public ArtificeMenu
{
public:
    ArtificeMenu_SpellSlots() : ArtificeMenu()
    {
        SetTitle("Spell Slots");
    }

    void Display(LiquidCrystal_I2C *lcd) override
    {
        // Get copy of vector of spells slot pointers
        auto character = MainCharacter::GetInstance()->GetCharacter();
        Serial.print("Character: ");
        Serial.println(character->characterName);
        spellSlots = character->spellSlots;

        lcd->clear();
        lcd->blink_off();
        DisplayFirstLine(lcd);
        DisplaySecondLine(lcd);
    }
    void IndicatePrevious() override
    {
        if (isEditing)
        {
            auto slot = spellSlots[currentSlotIndex];
            slot->currentSlots += 1;
        }
        else
            currentSlotIndex = currentSlotIndex > 0 ? currentSlotIndex - 1 : currentSlotIndex;
    }
    void IndicateNext() override
    {
        if (isEditing)
        {
            auto slot = spellSlots[currentSlotIndex];
            slot->currentSlots -= 1;
        }
        else
            currentSlotIndex = currentSlotIndex < 9 ? currentSlotIndex + 1 : currentSlotIndex;
    }
    void Select() override
    {
        if(!isEditing)
            isEditing = true;
        else
            ArtificeMenu::Select();
    }
    void Back() override
    {
        if(isEditing)
            isEditing = false;
        else
            ArtificeMenu::Back();
    }

private:
    SimpleVector<SpellSlot *> spellSlots;
    byte currentSlotIndex = 0;
    bool isEditing = false;

    void DisplayFirstLine(LiquidCrystal_I2C *lcd)   
    {
        auto slot = spellSlots[currentSlotIndex];

        Serial.println("Current Index: ");
        Serial.println(currentSlotIndex);

        Serial.println("Slots: ");
        Serial.println(slot->level);
        Serial.println(slot->currentSlots);
        Serial.println(slot->maxSlots);

        lcd->setCursor(0, 0);
        lcd->print("LvL ");
        lcd->print(slot->level);
        lcd->print(" ");
        lcd->print(slot->currentSlots);
        lcd->print("/");
        lcd->print(slot->maxSlots);
    }
    void DisplaySecondLine(LiquidCrystal_I2C *lcd)
    {
        auto slot = spellSlots[currentSlotIndex];

        lcd->setCursor(0, 1);
        lcd->print("[");
        for (byte i = 0; i < slot->maxSlots; i++)
        {
            if (i < slot->currentSlots)
                lcd->write(FULL_BLOCK_INDEX);
            else
                lcd->print(" ");
        }
        lcd->print("]");

        if(isEditing){
            lcd->setCursor(currentSlotIndex + 1, 1);
            lcd->blink_on();
        }
    }
};

#endif