#ifndef ARTIFICE_MENU_INTRO_SCREEN_H
#define ARTIFICE_MENU_INTRO_SCREEN_H

#include <Arduino.h>
#include <ArtificeMenu.h>
#include <LiquidCrystal_I2C.h>

class ArtificeMenu_IntroScreen : public ArtificeMenu{

public:
    ArtificeMenu_IntroScreen() : ArtificeMenu(){
        SetTitle("Intro Screen");

        AddSubmenu(new ArtificeMenu_Home());
    }
    
    void Display(LiquidCrystal_I2C *lcd) override
    {
        lcd->clear();

        if (isSubmenuOpen)
        {
            auto submenu = submenus[currentSelection];
            submenu->Display(lcd);
            return;
        }

        lcd->setCursor(0, 0);
        lcd->print("----Artifice----");
        lcd->setCursor(0, 1);
        lcd->print("------DnD-------");
    }

private:

};


#endif