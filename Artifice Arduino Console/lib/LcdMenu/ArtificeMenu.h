#ifndef LCD_MENU_H
#define LCD_MENU_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <ArtificeLcdCharacters.h>
#include <InputManager.h>
#include <SimpleVector.h>
#include <SoundManager.h>

class ArtificeMenu
{
public:
    ArtificeMenu()
    {
        submenus = SimpleVector<ArtificeMenu *>();
    }

    void virtual Update(LiquidCrystal_I2C *lcd)
    {
        OptimizeBacklight(lcd);

        // Do not update if you dont have new input
        if (InputManager::getInstance().HasInput())
        {
            lastInputTime = millis();
            HandleInput();
            Display(lcd);
        }
    }
    void virtual HandleInput()
    {
        auto upValue = GetInput(InputType::Up);
        auto downValue = GetInput(InputType::Down);
        auto aValue = GetInput(InputType::A);
        auto bValue = GetInput(InputType::B);

        if (upValue == HIGH)
            IndicatePrevious();
        else if (downValue == HIGH)
            IndicateNext();
        else if (aValue == HIGH)
            Select();
        else if (bValue == HIGH)
            Back();
    }
    void virtual Display(LiquidCrystal_I2C *lcd)
    {
        if (isSubmenuOpen)
        {
            auto submenu = submenus[currentSelection];
            submenu->Display(lcd);
            return;
        }

        lcd->clear();

        topRowIndex = currentSelection;
        if (topRowIndex > 0 && topRowIndex == submenus.GetSize() - 1)
            topRowIndex--;

        for (byte i = topRowIndex; i < topRowIndex + 2 && i < submenus.GetSize(); i++)
        {
            ArtificeMenu *item = submenus[i];
            lcd->setCursor(0, i - topRowIndex);
            lcd->print(item->GetTitle());
        }

        ShowIndicator(lcd);
    }
    void virtual Select()
    {
        if (isSubmenuOpen)
        {
            submenus[currentSelection]->Select();
            return;
        }

        // Else
        SoundManager::getInstance().Select();
        if (submenus.GetSize() > 0)
        {
            isSubmenuOpen = true;
        }
        else
        {
            // SoundManager::getInstance().Error();
        }
    }
    void virtual Back()
    {
        if (isSubmenuOpen)
        {
            submenus[currentSelection]->Back();
            return;
        }

        SoundManager::getInstance().Back();
        if (parent != NULL)
        {
            parent->isSubmenuOpen = false;
        }
    }
    void virtual IndicatePrevious()
    {
        Serial.println("previous");
        if (isSubmenuOpen)
        {
            submenus[currentSelection]->IndicatePrevious();
            return;
        }

        if (currentSelection > 0)
            --currentSelection;
    }
    void virtual IndicateNext()
    {
        if (isSubmenuOpen)
        {
            submenus[currentSelection]->IndicateNext();
            return;
        }

        if (currentSelection < submenus.GetSize() - 1)
            ++currentSelection;
    }

    void AddSubmenu(ArtificeMenu *submenu)
    {
        submenus.PushBack(submenu);
        submenu->parent = this;
    }
    const char *GetTitle()
    {
        return title;
    }
    void SetTitle(const char *title)
    {
        this->title = title;
    }

protected:
    const char *title = "Undefined";

    ArtificeMenu *parent = NULL;
    SimpleVector<ArtificeMenu *> submenus;

    // Handle selections
    byte topRowIndex = 0;
    byte currentSelection = 0;
    bool isSubmenuOpen = false;

protected:
    // Utility methods
    void ShowIndicator(LiquidCrystal_I2C *lcd)
    {
        lcd->setCursor(15, currentSelection - topRowIndex);
        lcd->write(INDICATOR_INDEX);
    }
    void ClearLcd(LiquidCrystal_I2C *lcd)
    {
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                lcd->setCursor(i, j);
                lcd->print(" ");
            }
        }
    }

private:
    // Handle inactivity
    bool currentBacklightState = true;
    const unsigned long inactiveTime = 7500;
    unsigned long lastInputTime = 0;

    void OptimizeBacklight(LiquidCrystal_I2C *lcd)
    {
        if (currentBacklightState && !IsActive())
            SetBacklight(lcd, false);
        if (!currentBacklightState && IsActive())
            SetBacklight(lcd, true);
    }
    bool IsActive()
    {
        long currentTime = millis();
        return currentTime - lastInputTime <= inactiveTime;
    }
    void SetBacklight(LiquidCrystal_I2C *lcd, bool state)
    {
        currentBacklightState = state;

        if (state)
            lcd->backlight();
        else
            lcd->noBacklight();
    }
};

#endif