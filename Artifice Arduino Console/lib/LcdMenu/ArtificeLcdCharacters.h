#ifndef ARTIFICE_LCD_CHARACTERS_H
#define ARTIFICE_LCD_CHARACTERS_H

#define FULL_BLOCK_INDEX        0
#define INDICATOR_INDEX         1

#define HP_ICON_INDEX           6
#define SPELLS_ICON_INDEX       7

#endif