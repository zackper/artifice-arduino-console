#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

// Interface for objects that can be serialized/deserialized
class Serializable {
public:
    virtual int GetFixedSize() = 0;
    virtual void Serialize(int address) = 0;
    virtual void Deserialize(int address) = 0;
};

#endif