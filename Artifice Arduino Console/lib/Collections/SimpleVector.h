#ifndef SIMPLE_VECTOR_H
#define SIMPLE_VECTOR_H

template <typename T>
class SimpleVector
{
private:
    T *array;
    size_t capacity;
    size_t size;

public:
    SimpleVector() : array(nullptr), capacity(0), size(0) {}

    ~SimpleVector()
    {
        delete[] array;
    }

    void PushBack(const T &value)
    {
        if (size >= capacity)
        {
            reserve(capacity == 0 ? 1 : capacity * 2);
        }
        array[size++] = value;
    }
    void PushFront(const T &value)
    {
        if (size >= capacity)
        {
            reserve(capacity == 0 ? 1 : capacity * 2);
        }
        for (size_t i = size; i > 0; --i)
        {
            array[i] = array[i - 1];
        }
        array[0] = value;
        size++;
    }
    void Remove(size_t index)
    {
        if (index >= size)
        {
            // Handle out-of-bounds access (you may choose to throw an exception or handle it differently)
            Serial.println("Error: Index out of bounds.");
            return;
        }
        for (size_t i = index; i < size - 1; ++i)
        {
            array[i] = array[i + 1];
        }
        size--;
    }
    size_t GetSize() const
    {
        return size;
    }

    T &operator[](size_t index)
    {
        if (index >= size)
        {
            // Handle out-of-bounds access (you may choose to throw an exception or handle it differently)
            Serial.println("Error: Index out of bounds.");
            index = size - 1;
        }
        return array[index];
    }

private:
    void reserve(size_t newCapacity)
    {
        T *newArray = new T[newCapacity];
        for (size_t i = 0; i < size; ++i)
        {
            newArray[i] = array[i];
        }
        delete[] array;
        array = newArray;
        capacity = newCapacity;
    }
};

#endif