#ifndef INPUT_COMPONENT_H
#define INPUT_COMPONENT_H

class InputComponent
{
public:
    byte pin;
    bool isStateful = false;

    InputComponent() {}
    InputComponent(byte pin)
    {
        this->pin = pin;
    }

    virtual int GetValue()
    {
        return cachedValue;
    }
    virtual void UpdateValue() = 0;

protected:
    int16_t cachedValue = 0;
};

#endif