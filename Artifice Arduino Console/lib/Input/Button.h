#ifndef BUTTON_H
#define BUTTON_H

#include <Arduino.h>
#include <InputComponent.h>

class Button : public InputComponent
{
public:
    Button() {}
    Button(byte pin) : InputComponent(pin)
    {
        pinMode(pin, INPUT);
    }

    bool IsPressed()
    {
        int value = digitalRead(pin);
        return value == HIGH;
    }

    void UpdateValue() override
    {
        auto value = digitalRead(pin);

        if (value == LOW && pressed)
        {
            pressed = false;
            cachedValue = 0;
        }
        else if (value == HIGH && !pressed)
        {
            pressed = true;
            pressedTime = millis();
            cachedValue = 1;
        }
        else if (value == HIGH && pressed)
        {
            if (millis() - pressedTime > pressedTimeToReactivate)
                cachedValue = 1;
            else
                cachedValue = 0;
        }
    }

protected:
    bool pressed = false;
    unsigned long pressedTime = 0;
    uint16_t pressedTimeToReactivate = 1000;
};

#endif