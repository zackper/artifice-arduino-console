#ifndef SLIDESWITCH_H
#define SLIDESWITCH_H

#include <Arduino.h>
#include <InputComponent.h>

class SlideSwitch : public InputComponent
{
public:
    SlideSwitch() {}
    SlideSwitch(byte pin) : InputComponent(pin)
    {
        isStateful = true;
        pinMode(pin, INPUT);
    }

    void UpdateValue() override
    {
        cachedValue = digitalRead(pin) == HIGH;
    }

protected:
};

#endif