#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

#include <Arduino.h>
#include <InputComponent.h>

class Potentiometer : public InputComponent
{
public:
    Potentiometer() {}
    Potentiometer(byte pin) : InputComponent(pin)
    {
        pinMode(pin, INPUT);
    }

    void UpdateValue() override
    {
        cachedValue = analogRead(pin);
    }

protected:

private:
};

#endif
