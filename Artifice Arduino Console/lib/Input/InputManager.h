#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#define GetInput(X) InputManager::getInstance().GetInputValue(X)

#include <SimpleVector.h>
#include <InputComponent.h>

enum InputType
{
    Down,
    Up,
    B,
    A,
    SoundSwitch,
};
struct InputData
{
    InputType type;        // Type of input
    InputComponent *component; // Input class
};

class InputManager
{
public:
    static InputManager &getInstance()
    {
        static InputManager instance;
        return instance;
    }

    void AddInput(InputType type, InputComponent *input)
    {
        InputData *inputData = new InputData();
        inputData->type = type;
        inputData->component = input;
        inputs.PushBack(inputData);
    }
    int GetInputValue(InputType type)
    {
        for (byte i = 0; i < inputs.GetSize(); i++)
        {
            if (inputs[i]->type == type)
                return inputs[i]->component->GetValue();
        }

        return 0;
    }
    bool HasInput()
    {
        for (byte i = 0; i < inputs.GetSize(); i++)
        {
            auto input = inputs[i];
            if (input->component->isStateful == false && input->component->GetValue() > 0)
                return true;
        }
        return false;
    }
    void UpdateInput()
    {
        for (byte i = 0; i < inputs.GetSize(); i++)
        {
            inputs[i]->component->UpdateValue();
        }
    }

private:
private:
    SimpleVector<InputData *> inputs;

    // Singleton Logic
    InputManager()
    {
        // Constructor code (if needed)
    }
    ~InputManager()
    {
        // Destructor code (if needed)
    }
    InputManager(const InputManager &) = delete;
    InputManager &operator=(const InputManager &) = delete;
};

#endif