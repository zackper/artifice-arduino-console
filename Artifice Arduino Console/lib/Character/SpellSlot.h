#ifndef SPELL_SLOT_H
#define SPELL_SLOT_H

struct SpellSlot
{
    byte level;
    byte currentSlots;
    byte maxSlots;

    SpellSlot(byte level, byte currentSlots, byte maxSlots)
    {
        this->level = level;
        this->currentSlots = currentSlots;
        this->maxSlots = maxSlots;
    }
};

#endif