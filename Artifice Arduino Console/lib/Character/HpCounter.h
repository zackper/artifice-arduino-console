#ifndef HP_COUNTER_H
#define HP_COUNTER_H

#include <EEPROM.h>

struct HpCounter
{
    int16_t current;
    int16_t max;

    HpCounter()
    {
        current = 6;
        max = 10;
    }
    byte GetPercent()
    {
        if (current < 0)
            return 0;
        else
            return (current * 100) / max;
    }

};

#endif