#ifndef CHARACTER_H
#define CHARACTER_H

#include <HpCounter.h>
#include <SpellSlot.h>
#include <Serializable.h>
#include <EEPROM.h>

#define SIZE_NAME           20
#define SIZE_CLASS_NAME     15
#define SIZE_HP             4
#define SIZE_SPELL_SLOT     2
#define MAX_SPELL_LEVEL     9
#define SIZE_LEVEL          1
#define SIZE_BARDIC_MUSIC   1

#define CHARACTER_SIZE (SIZE_NAME + SIZE_CLASS_NAME + SIZE_HP + SIZE_SPELL_SLOT + (SIZE_LEVEL * MAX_SPELL_LEVEL) + SIZE_BARDIC_MUSIC)

#define SELECTED_CHARACTER_ID 0
#define FIRST_CHARACTER_ADDRESS 10

#define LUTHER_ID 0
#define ALBADAR_ID 1

class Character
{
public:
    byte characterId;
    char characterName[SIZE_NAME];
    char className[SIZE_CLASS_NAME];    

    byte level;

    HpCounter *hp;
    SimpleVector<SpellSlot *> spellSlots;

    byte bardicPoints;

    Character()
    {
        strcpy(characterName, "Undefined Name");
        strcpy(className, "Undefined Class");
        level = 1;
        hp = new HpCounter();
    }

    void Save(){
        auto address = FIRST_CHARACTER_ADDRESS + SELECTED_CHARACTER_ID * CHARACTER_SIZE;
        
        // Save character name
        for (size_t i = 0; i < sizeof(characterName); i++) {
            EEPROM.write(address++, (int8_t)characterName[i]);
        }

        // Save class name
        for (size_t i = 0; i < sizeof(className); i++) {
            EEPROM.write(address++, (int8_t)className[i]);
        }

        // Save level
        EEPROM.put(address, level);
        address += SIZE_LEVEL;

        // Save HP
        EEPROM.put(address, *hp);
        address += SIZE_HP;

        // Save Spell Slots
        for(int i = 0; i < MAX_SPELL_LEVEL; i++){
            EEPROM.put(address++, spellSlots[i]->currentSlots);
            EEPROM.put(address++, spellSlots[i]->maxSlots);
        }

        // Save bardic points
        EEPROM.put(address, bardicPoints);
        address += SIZE_BARDIC_MUSIC;
    }
    void Load(){
        auto address = FIRST_CHARACTER_ADDRESS + SELECTED_CHARACTER_ID * CHARACTER_SIZE;
        
        // Load character name
        for (size_t i = 0; i < sizeof(characterName); i++) {
            characterName[i] = EEPROM.read(address++);
        }

        // Load class name
        for (size_t i = 0; i < sizeof(className); i++) {
            className[i] = EEPROM.read(address++);
        }

        // Load level
        EEPROM.get(address, level);
        address += SIZE_LEVEL;

        // Load HP
        EEPROM.get(address, *hp);
        address += SIZE_HP;

        // Load Spell Slots
        for(int i = 0; i < MAX_SPELL_LEVEL; i++){
            EEPROM.get(address++, spellSlots[i]->currentSlots);
            EEPROM.get(address++, spellSlots[i]->maxSlots);
        }

        // Load bardic points
        EEPROM.get(address, bardicPoints);
        address += SIZE_BARDIC_MUSIC;
    }

private:
};

/**
 * The MainCharacter class manages the main character instance using the Singleton pattern.
 */
class MainCharacter
{
protected:
    Character *character = nullptr;

    MainCharacter()
    {
    }

    static MainCharacter *_instance;

public:
    // Singleton Pattern
    MainCharacter(MainCharacter &other) = delete;
    void operator=(const MainCharacter &) = delete;
    static MainCharacter *GetInstance();

    // Get/Set character
    void SetCharacter(Character *character)
    {
        this->character = character;
    }
    Character *GetCharacter()
    {
        return character;
    }
};

// Initialize static member
MainCharacter *MainCharacter::_instance = nullptr;

// Static method definition must be inside the class
MainCharacter *MainCharacter::GetInstance()
{
    if (_instance == nullptr)
    {
        _instance = new MainCharacter();
    }
    return _instance;
}

#endif
