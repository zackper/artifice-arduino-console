#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#define GetInput(X) InputManager::getInstance().GetInputValue(X)

#include <SimpleVector.h>
#include <InputManager.h>
#include <NotesUtility.h>
#include <MusicPlayer.h>

class SoundManager
{
public:
    static SoundManager &getInstance()
    {
        static SoundManager instance;
        return instance;
    }

    // Update to handle pseudoparallel music with tone
    void Update(long currentTime)
    {
        musicPlayer.Update(currentTime);
    }

    // Presets of music
    void Select()
    {
        if (IsEnabled())
        {
            tone(buzzerPin, 1200, 50); // Fast, positive sound for click
        }
    }
    void Back()
    {
        if (IsEnabled())
        {
            tone(buzzerPin, 400, 50); // Negative sounding sound for back
        }
    }
    void RestoreHP()
    {
        if (IsEnabled())
        {
            auto clip = new MusicClip();
            clip->notes.PushBack(MusicNote(NOTE_D4, 100));
            clip->notes.PushBack(MusicNote(NOTE_E4, 100));
            clip->notes.PushBack(MusicNote(NOTE_A4, 100));
            clip->notes.PushBack(MusicNote(NOTE_G4, 100));
            clip->notes.PushBack(MusicNote(NOTE_FS4, 200));

            musicPlayer.PlayMusicClip(clip);
        }
    }
    void LoseHP()
    {
        if (IsEnabled())
        {
            tone(buzzerPin, 200, 300); // Low, intense sound for low damage
        }
    }
    void SpellCast()
    {
        if (IsEnabled())
        {
            tone(buzzerPin, 1500, 500); // Magical sound for spell cast
        }
    }

    void stopSound()
    {
        noTone(buzzerPin); // Turn off the buzzer
    }

private:
    const byte buzzerPin = 7;
    MusicPlayer musicPlayer;

    bool IsEnabled()
    {
        return InputManager::getInstance().GetInputValue(InputType::SoundSwitch);
    }

private:
    // Singleton Logic
    SoundManager()
    {
        // Constructor code (if needed)
    }
    ~SoundManager()
    {
        // Destructor code (if needed)
    }
    SoundManager(const SoundManager &) = delete;
    SoundManager &operator=(const SoundManager &) = delete;
};

#endif
