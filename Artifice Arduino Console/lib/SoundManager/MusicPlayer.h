#ifndef MUSIC_PLAYER_H
#define MUSIC_PLAYER_H

#include <Arduino.h>

class MusicNote
{
public:
    short tone;
    short duration;

    MusicNote(){

    }
    MusicNote(short _tone, short _duration){
        tone = _tone;
        duration = _duration;
    }
};

class MusicClip
{
public:
    SimpleVector<MusicNote> notes;
};

class MusicPlayer
{

public:
    void PlayMusicClip(MusicClip *clip)
    {
        this->clip = clip;
        currentNoteIndex = -1;
        currentNoteStartTime = -1;
    }

    void Update(long currentTime)
    {
        if (clip == NULL)
            return;

        if (
            currentNoteIndex == -1 ||
            currentTime - currentNoteStartTime > clip->notes[currentNoteIndex].duration
        )
        {
            // Quick way to skip comparison cast
            if(currentNoteIndex + 1 < clip->notes.GetSize() + 1)    
                PlayNextNote(currentTime);
            else
                noTone(buzzerPin);
        }
    }

private:
    byte buzzerPin = 7;

    MusicClip *clip;
    short currentNoteIndex;
    short currentNoteStartTime;

    
    void PlayNextNote(long currentTime)
    {
        if (clip == NULL)
            return;

        ++currentNoteIndex;
        currentNoteStartTime = currentTime;

        // Play tone
        auto note = clip->notes[currentNoteIndex];
        tone(buzzerPin, note.tone, note.duration);
    }
};

#endif